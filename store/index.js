import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		text: '测试',
		WXuserInfo: ''
	},
    mutations: {
		SetWXuserInfo(state,data) {
			state.WXuserInfo = data;
			uni.showToast({
				title: state.WXuserInfo,
				icon: 'none'
			})
		},
    }
})

export default store